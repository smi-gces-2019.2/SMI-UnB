from django.contrib.auth.decorators import user_passes_test, login_required
from django.shortcuts import render, get_object_or_404

from smi_unb.campuses.models import Campus


@login_required
def index(request):
    campuses = Campus.objects.all()

    return render(request, 'campuses/index.html', {'campuses': campuses})


@login_required
def campus_index(request, campus_string):
    campus = get_object_or_404(Campus, url_param=campus_string)

    buildings = campus.get_all_active_buildings()
    inactive_buildings = bool(campus.get_all_inactive_buildings())

    return render(
        request,
        'campuses/campus_index.html',
        {
            'campus': campus,
            'buildings': buildings,
            'inactive_buildings': inactive_buildings
        }
    )


@login_required
def campus_info(request, campus_string):
    campus = get_object_or_404(Campus, url_param=campus_string)

    return render(request, 'campuses/campus_info.html', {'campus': campus})


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_buildings"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def inactive_buildings(request, campus_string):
    campus = get_object_or_404(Campus, url_param=campus_string)

    inactive_buildings = campus.get_all_inactive_buildings()

    return render(
        request,
        'campuses/inactive_buildings.html',
        {
            'campus': campus,
            'inactive_buildings': inactive_buildings
        }
    )
