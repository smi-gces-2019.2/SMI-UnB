from django.contrib.auth.models import User
from django.test import TestCase

from smi_unb.authentication.forms import LoginForm


class UsersFormTest(TestCase):
    def setUp(self):
        self.superuser = User(
            username='test',
            first_name='superfirst',
            last_name='superlast',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

        self.user = User(
            username='normaluser',
            first_name='normalfirst',
            last_name='normallast',
            email="test@test.com",
            is_superuser=False,
        )
        self.user.set_password('12345')
        self.user.save()

    def test_correct_login_form(self):
        data = {
            'email': self.superuser.email,
            'password': '12345',
        }

        form = LoginForm(instance=self.superuser, data=data)
        self.assertTrue(form.is_valid())

    def test_login_form_with_wrong_email(self):
        data = {
            'email': 'non_existent_email@test.com',
            'password': '12345',
        }

        form = LoginForm(instance=self.superuser, data=data)
        self.assertFalse(form.is_valid())

    def test_login_form_with_wrong_email_type(self):
        data = {
            'email': 'wrongemail',
            'password': '12345',
        }

        form = LoginForm(instance=self.superuser, data=data)
        self.assertFalse(form.is_valid())

    def test_login_form_with_wrong_password(self):
        data = {
            'email': self.superuser.email,
            'password': 'wrongpassword',
        }

        form = LoginForm(instance=self.superuser, data=data)
        self.assertFalse(form.is_valid())
