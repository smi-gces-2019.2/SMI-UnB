from django import forms
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User


class LoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'password', ]

    email = forms.CharField(
        label="E-mail",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'email',
                'size': 30,
                'type': 'text'
            }
        )
    )

    password = forms.CharField(
        label="Senha",
        max_length=16,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'password',
                'size': 16,
                'type': 'password'
            }
        )
    )

    def is_valid(self):
        valid = super(LoginForm, self).is_valid()

        if not valid:
            return False

        try:
            user = User.objects.get(email=self.cleaned_data['email'])
        except User.DoesNotExist:
            self.add_error(None, 'Email ou Senha Inválidos.')
            return False

        if not check_password(self.cleaned_data['password'], user.password):
            self.add_error(None, 'Email ou Senha Inválidos.')
            return False

        return True
